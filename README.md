# WSEG Skeleton

This is a skeleton used for the practice projects in the WSEG course at the Bern University of Applied Sciences.

## Getting Started

This is a shortcut we can use because we allready have angular fire installed.

### Connect your App to firebase

**Prerequisites:** Forked Repository, you have your local clone. ng serve works and shows you the login page

1. Visit Firebase Console [Console](console.firebase.google.com)
2. Create new Project, for example wseg-4x-groupname
3. Activate Authentication
   1. Enable Native Email Password Auth
   2. Create a testuser, ex: user@example.com 123456
4. Activate Firestore Database
   1. Test: Works for 30 days, Prod: Works forever but you have to change a security rule
   2. We choose prod and location eu
   3. Create collection
      1. id: user-data and one
      2. first document
         1. Click on automatic generated id
         2. Add a field called name with a value: foobar
5. Go to project overview
   1. Click on add app
   2. Enter a good name
   3. Click on Register app(App registrieren)
   4. DON'T!!! install firebase
   5. Copy the firebase config to your environments\*.ts file and replace the existing config
6. Start App with ng serve
7. Try to log in with your testuser -> Login should be successful
   1. If you want to test if the auth works -> Deactivate the user in the firebase console
   2. Open a private browser, try to log in -> User should be denied
8. Visit User-Settings
   1. Create a new entry
   2. Reload page: if you have setup the firestore as prod the entry should not be displayed.
   3. Go to firestore console and change the securityrules.
      1. change the if false to if req.auth != null
      2. Wait a couple of minutes...
      3. Rule should be in place and valid
   4. Try to create a new entry
   5. Reload page -> The entry should be there now.
   6. Visit firestore, the entry should be there now
9. You have successfully connected your app to firebase

### Host your app with firebase

### Host your app with gitlab CI/CD

## Set up the Project

1. Each group member: Create a GitLab Account
2. Each group member: Login with your GitLab Account
3. Only ONCE per group: Fork this repository (top-right: click on "Fork")
4. Each group member: Clone your forked group repository locally in your development environment

## Run the project from your terminal

Each group member:

1. Make sure you have Node and npm installed (https://nodejs.org/)
2. Make sure you have the Angular CLI installed (https://angular.io/cli)
3. Go to frontend (with `cd frontend`)
4. Run `npm install` to install the dependencies
5. Run `ng serve` to start up the development server
6. Visit the site on http://localhost:4200/

Now you should be ready to go :)

## Set up Firebase

Only ONCE per group:

1. Sign up for firebase on https://firebase.google.com
2. Create a new project (you can disable Google Analytics)
3. In the bottom left corner click on "Upgrade" to upgrade to the Pay-As-You-Go Plan. Follow the instructions. You
   should be way within the Free Plan, so no actual costs should be charged to you.
4. Under Build / Authentication:
   1. Click "Get Started"
   2. Go to "Sign-in method"
   3. Add e-mail / password as new sign-in provider
5. Under Build / Firestore Database
   1. Click "Get Started"
   2. Click "Create Database"
   3. Select "Test Mode" and click "Next"
   4. Select a European Cloud Firestore Region and click "Activate"
   5. Create a new Collection named "user-data"
   6. Click "Automatically generated ID" and click "Save"
6. Under Build / Hosting
   1. Click "Get Started"
   2. Follow the instructions
   3. Run `firebase login:ci` to get a Firebase Token
   4. Copy the token to the clipboard
   5. Copy the URL of your project from the Firebase Console (under Hosting)
   6. In your local repository open the file (.gitlab-ci.yml) and replace the URL https://demoprojekt-wseg.web.app/
      everywhere with your own URL. Also replace "demoprojekt-wseg" everywhere with your own project ID (should be the
      string between "https://" and ".web.app".
   7. Also replace "demo-projekt-wseg" with your project ID everywhere in [frontend/.firebaserc](frontend/.firebaserc),
      in [environment.ts](frontend/src/environments/environment.ts) and
      in [environment.prod.ts](frontend/src/environments/environment.prod.ts).
   8. Build and deploy the project locally by running `npm install`, `ng build` and `firebase deploy`
7. On Project Overview
   1. Add a new Web app
   2. Name it with your project ID
   3. Then copy the content of firebaseConfig to your clipboard and replace the firebaseConfig in your environment
      files with the copied content.
8. Next to Project Overview click on the Cogwheel
   1. Under Users and Permissions you can add your Team Members so they can also interact with the firebase console.
9. In your GitLab project go to Settings / CI/CD
   1. Expand the section "Variables"
   2. Add a new Variable with the following properties
      - Key: FIREBASE_TOKEN
      - Value: paste the firebase token you copied to the clipboard in the previous step
      - Type: Variable
      - Environment Scope: All
      - Protect Variable: unchecked
      - Mask Variable: checked
